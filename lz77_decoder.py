#!/usr/bin/env python

# Refences:
# [1] https://learn.microsoft.com/en-us/openspecs/windows_protocols/ms-wusp/fb98aa28-5cd7-407f-8869-a6cef1ff1ccb
# [2] https://github.com/oneapi-src/oneAPI-samples/tree/master/DirectProgramming/C%2B%2BSYCL_FPGA/ReferenceDesigns/decompress

# on_chip_cache.py

import numpy as np
import pyopencl as cl
import pyopencl.cltypes
import os, sys, getopt
import struct

# -------------------------------------- parse arguments --------------------------------------
board_name = 'em'
aocx_version = ''
test_id = 0
LZ77_n_literals = 4

try:
    opts, args = getopt.getopt(sys.argv[1:],"hb:v:t",["board=","version=","test=",])
except getopt.GetoptError:
    print('lz77_decoder.py -b <board-name> -v <version-aocx> -t <test-id>')
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print('lz77_decoder.py -b <board-name> -v <version-aocx> -t <test-id>')
        sys.exit()
    elif opt in ("-b", "--board"):
        board_name = arg
    elif opt in ("-v", "--version"):
        aocx_version = arg
    elif opt in ("-t", "--test"):
        test_id = int(arg)

print('borad-name:', board_name)
print('version-aocx:', aocx_version)
print('test:', test_id)
# ---------------------------------------------------------------------------------------------

AOCX_BIN_PATH = os.path.join('./build', board_name)
AOCX_BIN_NAME = 'lz77_decoder' + aocx_version + '.aocx'

def LZ77_InDataT(literal, dist, length, valid_count, is_literal, flag):
    return np.frombuffer(struct.pack('<'+'b'*LZ77_n_literals+'HHbbb', *literal, dist, length, valid_count, is_literal, flag), dtype=np.ubyte)

def LZ77_OutDataT(data):
    _t = struct.unpack('<'+'b'*LZ77_n_literals+'bb', data)
    return _t[:LZ77_n_literals], _t[LZ77_n_literals], _t[LZ77_n_literals+1]>0 # data, valid_count, flag

def test0_4():
    d0 = LZ77_InDataT(literal=bytes('Axxx', 'utf-8'), dist=0, length=0, valid_count=1, is_literal=True, flag=False)
    d1 = LZ77_InDataT(literal=bytes('xxxx', 'utf-8'), dist=1, length=1, valid_count=0, is_literal=False, flag=False)
    d2 = LZ77_InDataT(literal=bytes('Bxxx', 'utf-8'), dist=0, length=0, valid_count=1, is_literal=True, flag=False)
    d3 = LZ77_InDataT(literal=bytes('Cxxx', 'utf-8'), dist=0, length=0, valid_count=1, is_literal=True, flag=False)
    d4 = LZ77_InDataT(literal=bytes('xxxx', 'utf-8'), dist=2, length=1, valid_count=0, is_literal=False, flag=False)
    d5 = LZ77_InDataT(literal=bytes('xxxx', 'utf-8'), dist=1, length=1, valid_count=0, is_literal=False, flag=False)
    d6 = LZ77_InDataT(literal=bytes('xxxx', 'utf-8'), dist=5, length=3, valid_count=0, is_literal=False, flag=False)
    data_in = np.concatenate((d0,d1,d2,d3,d4,d5,d6,))
    data_in[-1] = 1
    data_out = np.frombuffer(bytes('AABCBBABC', 'utf-8'), dtype=np.ubyte)
    return data_in, data_out

# --- single OpenCL execution  ---
def lz77_decoder_cl(data_in, olen=None):
    """Single OpenCL kernel run"""

    import pyopencl as cl
    import os
    
    # a helper function to create aligned numpy arrays    
    def np_aligned_empty(shape, dtype, alignbytes):
        # https://stackoverflow.com/questions/9895787/memory-alignment-for-fast-fft-in-python-using-shared-arrays
        dtype = np.dtype(dtype)
        nbytes = np.prod(shape) * dtype.itemsize
        buf = np.empty(nbytes+alignbytes, dtype=np.uint8)
        start_idx = -buf.ctypes.data % alignbytes
        return buf[start_idx:start_idx + nbytes].view(dtype).reshape(shape)

    # --- wrap input data into aligned arrays on host ---
    item_sz = LZ77_InDataT(literal=bytes('ABCD', 'utf-8'), dist=0, length=0, valid_count=4, is_literal=True, flag=True).size
    otem_sz = LZ77_n_literals + 2
    ilen = data_in.size // item_sz
    if olen in [None,[],-1]:
        olen = ilen

    # data type of control arrays must fit the kernel
    idata_h = np_aligned_empty((ilen*item_sz,), np.ubyte, 64); idata_h[()] = data_in
    odata_h = np_aligned_empty((olen*otem_sz,), np.ubyte, 64); odata_h[()] = 0

    # a standard opencl dance:
    # 1) load OpenCL image (source for GPU, binary for FPGA)
    # 2) create context
    # 3) create program
    # 4) get kernels
    # 5) create queue
    # 6) allocate space on device
    # 7) set kernel parameters
    # 7) copy input from the host memory to device
    # 8) enquque kernels
    # 9) copy result from device to host

    # --- load OpenCL source or binary ---
    
    # check emulation mode
    fpga_emulation = os.getenv('CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA')=='1'
    if(fpga_emulation):
        print("[azint-cl] FPGA emulation mode (ignore kernel programming error)")

    # load bitstream kernel
    aocx_filename = os.path.join(AOCX_BIN_PATH, AOCX_BIN_NAME)

    print("[azint-cl] OpenCL will use AOCX binary image: %s" % (aocx_filename,))

    # load bitstream kernel
    with open( aocx_filename, "rb") as fid:
        cl_binary = fid.read()

    # --- create context, program, kernels and queue ---
    
    # create some context
    ctx = cl.create_some_context()

    # OpenCL program
    prg = cl.Program(ctx, ctx.devices, [cl_binary,])

    # compute kernel(s)
    krn_in   = prg.LZ77DecoderMultiElement_test_in
    krn_out  = prg.LZ77DecoderMultiElement_test_out
    krn_lz77 = prg.LZ77DecoderMultiElement

    # create OpenCL event queue(s)
    queue_in   = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
    queue_out  = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)
    queue_lz77 = cl.CommandQueue(ctx, properties=cl.command_queue_properties.PROFILING_ENABLE)

    # --- allocate space on device ---
    
    mf = cl.mem_flags
    # specific Intel/Altera memory flags
    amf = {'CL_CHANNEL_1_INTELFPGA': 0x00010000,
           'CL_CHANNEL_2_INTELFPGA': 0x00020000, # 0x00020000
           'CL_MEM_HETEROGENEOUS_INTELFPGA': 0x00080000,
           'CL_ALTERA_TIMESTAMP': 0x00090000 }    

    # enqueue copy can be done only after setting the kernel parameters !
    # Note: do not use mf.ALLOC_HOST_PTR !
    idata_d = cl.Buffer(ctx, mf.READ_ONLY | amf['CL_CHANNEL_1_INTELFPGA'], idata_h.size*idata_h.dtype.itemsize)
    odata_d = cl.Buffer(ctx, mf.WRITE_ONLY  | amf['CL_CHANNEL_1_INTELFPGA'], odata_h.size*odata_h.dtype.itemsize)

    # --- set kernel parameters ---
    # Note: This must be done before enqueue copy in order to enforce the altera memory flags
    krn_in.set_args( idata_d, np.uint32(ilen) )
    krn_out.set_args( odata_d, np.uint32(olen) )    

    # --- copy input from the host memory to device ---
    cl.enqueue_copy(queue_in, idata_d, idata_h, is_blocking=True)
    queue_in.finish()

    # --- enqueue kernels ---
    evt_lz77 = cl.enqueue_nd_range_kernel(queue_lz77, krn_lz77, (1,1,1), (1,1,1)) # single task kernel
    evt_in   = cl.enqueue_nd_range_kernel(queue_in,   krn_in,   (1,1,1), (1,1,1)) # single task kernel
    evt_out  = cl.enqueue_nd_range_kernel(queue_out,  krn_out,  (1,1,1), (1,1,1)) # single task kernel
    queue_lz77.flush()
    queue_in.flush()
    queue_out.flush()   
    cl.wait_for_events([evt_in,])
    cl.wait_for_events([evt_out,])
    cl.wait_for_events([evt_lz77,])

    # --- copy result from device to host ---
    cl.enqueue_copy(queue_out, odata_h, odata_d, is_blocking=True)

    # print some statistics
    #t_ms = (evt_hst.profile.end-evt_hst.profile.start)*1e-6
    #print("[azint-cl] exec. time transpose(1): %.2f ms, %.2f MHz" % (t_ms,ilen*1e-3/t_ms,))

    # release events
    evt_lz77, evt_in, evt_out = None, None, None

    # return result
    return odata_h

if __name__ == "__main__":
    idata, odata = test0_4()
    print(idata)
    odata_cl = lz77_decoder_cl(idata)
    print(odata_cl)

# ------------------------------------------------------------------------
# Usage: CL_CONTEXT_EMULATOR_DEVICE_INTELFPGA=1 PYOPENCL_CTX='1' python lz77_decoder.py



