//
// Performs LZ77 decoding for more than 1 element at once.
// Streams in 'LZ77InputData' (see common.hpp) appended with a flag (FlagBundle)
// which contains either a literal from upstream, or a {length, distance} pair.
// Given a literal input, this function simply tracks that literal in a history
// buffer and writes it to the output. For a {length, distance} pair, this
// function reads 'literals_per_cycle' elements from the history buffer per
// cycle and writes them to the output.
//
//  Template parameters:
//    InPipe: a SYCL pipe that streams in LZ77InputData with a boolean flag that
//      indicates whether the input stream is done.
//    OutPipe: a SYCL pipe that streams out an array of literals and a
//      'valid_count' that is in the range [0, literals_per_cycle].
//    literals_per_cycle: the number of literals to read from the history
//      buffer at once. This sets the maximum possible throughput for the
//      LZ77 decoder.
//    max_distance: the maximum distancefor a {length, distance} pair
//      For example, for DEFLATE this is 32K and for snappy this is 64K.
//

/* enable OpenCL extensions */
#pragma OPENCL EXTENSION cl_intel_channels : enable

// number of bits to count from 0 to literals_per_cycle-1
#define LZ77_history_buffer_buffer_idx_bits   2    /* log2(LZ77_literals_per_cycle) */
#define LZ77_literals_per_cycle  (1 << LZ77_history_buffer_buffer_idx_bits)
// exponent to get max_distance as a power of 2
#define LZ77_max_distance_exponent   16      /* log2(LZ77_max_distance) */
#define LZ77_max_distance        (1 << LZ77_max_distance_exponent)
// we will cyclically partition the history to 'literals_per_cycle' buffers,
// so each buffer gets this many elements
#define LZ77_history_buffer_count (LZ77_max_distance/LZ77_literals_per_cycle)
// bit mask for counting from 0 to literals_per_cycle-1
#define LZ77_history_buffer_buffer_idx_mask   (LZ77_literals_per_cycle - 1)
// number of bits to count from 0 to history_buffer_count-1
#define LZ77_history_buffer_idx_bits  (LZ77_max_distance_exponent - LZ77_history_buffer_buffer_idx_bits)  /* Log2(history_buffer_count) */
// bit mask for counting from 0 to history_buffer_count-1
#define LZ77_history_buffer_idx_mask  (LZ77_history_buffer_count - 1)

// TODO:
#define LZ77_max_literals LZ77_literals_per_cycle    /* max_literals <= literals_per_cycle */

// the data type used to index from 0 to literals_per_cycle-1 (i.e., pick
// which buffer to use)
#define LZ77_HistBufBufIdxT  uchar  /* uint(LZ77_history_buffer_buffer_idx_bits) */

// the data type used to index from 0 to history_buffer_count-1 (i.e., after
// picking which buffer, index into that buffer)
#define LZ77_HistBufIdxT   ushort /* uint(LZ77_history_buffer_idx_bits) */

// cache depth for on-chip history buffer
#define LZ77_kCacheDepth   8
 
typedef struct __attribute__ ((packed)) LZ77_InDataT
{
  unsigned char data_literal[LZ77_max_literals];
  unsigned short data_distance;
  unsigned short data_length;
  unsigned char data_valid_count;
  bool data_is_literal;
  bool flag;
} LZ77_InDataT;

typedef struct __attribute__ ((packed)) LZ77_OutDataT
{
  unsigned char data[LZ77_literals_per_cycle];
  unsigned char valid_count;
  bool flag;
} LZ77_OutDataT;

channel LZ77_InDataT   ch_lz77_in;
channel LZ77_OutDataT  ch_lz77_out;

/* Memory models for different board configurations */
#ifdef _USE_HBM_MEM_ /* using multiple HBM memories */
    #define ATTRIBUTE_MEN_BANK0 __attribute((buffer_location("HBM0")))
    #define ATTRIBUTE_MEN_BANK1 __attribute((buffer_location("HBM1")))
#else                /* BLANK is default */
    #define ATTRIBUTE_MEN_BANK0
    #define ATTRIBUTE_MEN_BANK1
#endif

__kernel void LZ77DecoderMultiElement_test_in( global ATTRIBUTE_MEN_BANK0 const LZ77_InDataT* restrict idata,
                                               const unsigned int ilen)
{
  for(uint i=0; i<ilen; i++) {
    LZ77_InDataT data = idata[i];
    write_channel_intel(ch_lz77_in, data);
  }
}

__kernel void LZ77DecoderMultiElement_test_out( global ATTRIBUTE_MEN_BANK1 LZ77_OutDataT* restrict odata,
                                                const unsigned int olen)
{
  for(uint i=0; i<olen; i++) {
    LZ77_OutDataT data = read_channel_intel(ch_lz77_out);
    odata[i] = data;  
  }
}

__kernel void LZ77DecoderMultiElement()
{
  // track whether we are reading from the history, and how many more elements
  // to read from the history
  bool reading_history = false;
  bool reading_history_next;
  short history_counter;

  // which of the 'literals_per_cycle' buffers is the one to write to next
  LZ77_HistBufBufIdxT history_buffer_buffer_idx = 0;

  // for each of the 'literals_per_cycle' buffers, where do we write next
  LZ77_HistBufIdxT __attribute__((register)) history_buffer_idx[LZ77_literals_per_cycle];

  // the OnchipMemoryWithCache history buffers cache in-flight writes to the 
  // history buffer and break loop carried dependencies that are smaller than
  // kCacheDepth

  // the history buffers
  uchar history_buffer[LZ77_history_buffer_count][LZ77_literals_per_cycle];

  uchar __attribute__((register)) history_buffer_cache[LZ77_kCacheDepth+1][LZ77_literals_per_cycle];
  ushort __attribute__((register)) history_buffer_cache_pos[LZ77_kCacheDepth+1][LZ77_literals_per_cycle];

  // these variables are used to read from the history buffer upon request from
  // the decoder kernel
  LZ77_HistBufBufIdxT read_history_buffer_buffer_idx = 0;
  LZ77_HistBufIdxT __attribute__((register)) read_history_buffer_idx[LZ77_literals_per_cycle];
  LZ77_HistBufBufIdxT __attribute__((register)) read_history_shuffle_idx[LZ77_literals_per_cycle];

  // precompute the function: dist + ((i - dist) % dist)
  // which is used for the corner case when the copy distance is less than
  // 'literals_per_cycle'
  uchar __attribute__((register)) mod_lut[LZ77_literals_per_cycle-1][LZ77_literals_per_cycle-1];
  for(int y = 0; y < (LZ77_literals_per_cycle-1); y++) {
    for(int x = y; x < (LZ77_literals_per_cycle-1); x++) {
      unsigned char dist = y + 1;
      unsigned char i = x + 1;
      mod_lut[y][x] = dist - ((i - dist) % dist);
    }
  } // TODO: This can be fully static (defined by preprocessor)

  // initialize the index pointers for each history buffer
  #pragma unroll
  for (int i = 0; i < LZ77_literals_per_cycle; i++) {
    history_buffer_idx[i] = 0;
  }

  bool done = false;

  // the main processing loop.
  // Using the OnchipMemoryWithCache, we are able to break all loop carried
  // dependencies with a distance of 'kCacheDepth' and less
  #pragma ivdep safelen(LZ77_kCacheDepth+1)
  while (!done) {
    bool data_valid = true;
    LZ77_OutDataT out_data;

    if (!reading_history) {
      // if we aren't currently reading from the history buffers,
      // then read from input pipe
      LZ77_InDataT pipe_data = read_channel_nb_intel(ch_lz77_in, &data_valid);

      // check if the upstream kernel is done sending us data
      done = pipe_data.flag && data_valid;

      // grab the literal or the length and distance pair
      unsigned short dist = pipe_data.data_distance;

      // for the case of literal(s), we will simply write it to the output
      // get the specific LZ77InputData type to see how many literals can come
      // in the input at once and that it is less than literals_per_cycle
      #pragma unroll
      for (int i = 0; i < LZ77_max_literals; i++) {
        out_data.data[i] = pipe_data.data_literal[i];
      }
      out_data.valid_count = pipe_data.data_valid_count;

      // if we get a length distance pair we will read 'pipe_data.data.length'
      // bytes starting at and offset of 'dist'
      history_counter = pipe_data.data_length;
      reading_history = !pipe_data.data_is_literal && data_valid;
      reading_history_next = history_counter > LZ77_literals_per_cycle;

      // grab the low Log2(literals_per_cycle) bits of the distance
      LZ77_HistBufBufIdxT dist_small = dist & LZ77_history_buffer_buffer_idx_mask;

      // find which of the history buffers we will read from first
      read_history_buffer_buffer_idx =
          (history_buffer_buffer_idx - dist_small) &
          LZ77_history_buffer_buffer_idx_mask;

      // find the starting read index for each history buffer, and compute the
      // shuffle vector for shuffling the data to the output
      #pragma unroll
      for (int i = 0; i < LZ77_literals_per_cycle; i++) {
        // the buffer index
        LZ77_HistBufBufIdxT buf_idx =
            (read_history_buffer_buffer_idx + (LZ77_HistBufBufIdxT)i) &
            LZ77_history_buffer_buffer_idx_mask;

        // compute the starting index for buffer 'buf_idx' (in range
        // [0, literals_per_cycle))
        LZ77_HistBufIdxT starting_read_idx_for_this_buf =
            (history_buffer_idx[buf_idx] - ((dist - i) / LZ77_literals_per_cycle)) - 1;
        if (buf_idx == history_buffer_buffer_idx) {
          starting_read_idx_for_this_buf += 1;
        }
        read_history_buffer_idx[buf_idx] = starting_read_idx_for_this_buf & LZ77_history_buffer_idx_mask;

        if (dist > i) {
          // normal case for ths shuffle vector
          read_history_shuffle_idx[i] = buf_idx;
        } else {
          // EDGE CASE!
          // this special case happens whenever dist < literals_per_cycle
          // and we need to repeat one of the earlier elements
          // idx_back = dist_small - ((i - dist_small) % dist_small));
          LZ77_HistBufBufIdxT idx_back = mod_lut[dist_small - 1][i - 1];
          read_history_shuffle_idx[i] = (history_buffer_buffer_idx - idx_back) & LZ77_history_buffer_buffer_idx_mask;
        } // if (dist > i)
      } // for (int i = 0; i < literals_per_cycle; i++)
    } // if (!reading_history)

    if (reading_history) {
      // grab from each of the history buffers
      unsigned char __attribute__((register)) historical_bytes[LZ77_literals_per_cycle];
      #pragma unroll
      for(int i=0; i<LZ77_literals_per_cycle; i++) {
        // get the index into this buffer and read from it
        LZ77_HistBufIdxT idx_in_buf = read_history_buffer_idx[i];
        uchar val = history_buffer[idx_in_buf][i];
        #pragma unroll
        for(uchar j=0; j<(LZ77_kCacheDepth+1); j++) {
          if(history_buffer_cache_pos[j][i] == idx_in_buf)
            val = history_buffer_cache[j][i];
        }
        historical_bytes[i] = val;
      }
      // shuffle the elements read from the history buffers to the output
      // using the shuffle vector computed earlier. Note, the numbers in the
      // shuffle vector need not be unique, which happens in the special case
      // of dist < literals_per_cycle, described above.
      #pragma unroll
      for (int i = 0; i < LZ77_literals_per_cycle; i++) {
        out_data.data[i] = historical_bytes[read_history_shuffle_idx[i]];
      }

      // we will write out min(history_counter, literals_per_cycle)
      // this can happen when the length of the copy is not a multiple of
      // 'literals_per_cycle'
      if (history_counter < LZ77_literals_per_cycle) {
        out_data.valid_count = history_counter;
      } else {
        out_data.valid_count = LZ77_literals_per_cycle;
      }

      // update the history read indices for the next iteration (if we are still
      // reading from the history buffers)
      #pragma unroll
      for (int i = 0; i < LZ77_literals_per_cycle; i++) {
        read_history_buffer_idx[i] =
            (read_history_buffer_idx[i] + (uchar)1) & LZ77_history_buffer_idx_mask;  // TODO: is this fast enough ?!
      }

      // update whether we will still be reading from the history buffers on
      // the next iteration of the loop
      reading_history = reading_history_next;
      reading_history_next = history_counter > LZ77_literals_per_cycle * 2;
      history_counter -= LZ77_literals_per_cycle;
    } // if (reading_history)

    if (!done && data_valid) {
      // compute the valid bitmap and shuffle vector for the writes
      bool __attribute__((register)) write_bitmap[LZ77_literals_per_cycle];
      LZ77_HistBufBufIdxT __attribute__((register)) shuffle_vec[LZ77_literals_per_cycle];

      #pragma unroll
      for (int i = 0; i < LZ77_literals_per_cycle; i++) {
        LZ77_HistBufBufIdxT buf_idx =
            (history_buffer_buffer_idx + i) & LZ77_history_buffer_buffer_idx_mask;
        write_bitmap[buf_idx] = i < out_data.valid_count;
        shuffle_vec[buf_idx] = i;
      }

      // write to the history buffers
      #pragma unroll
      for(int i=0; i<LZ77_literals_per_cycle; i++) {
        if (write_bitmap[i]) {
          // grab the literal to write out
          uchar literal_out = out_data.data[shuffle_vec[i]];

          // the index into this history buffer
          LZ77_HistBufIdxT idx_in_buf = history_buffer_idx[i];
          // history_buffer.template get<i>()[idx_in_buf] = literal_out;
          history_buffer[idx_in_buf][i] = literal_out;
          #pragma unroll
          for(uchar j=0; j<(LZ77_kCacheDepth); j++) {
            history_buffer_cache[j][i] = history_buffer_cache[j+1][i];
            history_buffer_cache_pos[j][i] = history_buffer_cache_pos[j+1][i];
          }
          history_buffer_cache[LZ77_kCacheDepth][i] = literal_out;
          history_buffer_cache_pos[LZ77_kCacheDepth][i] = idx_in_buf;

          // update the history buffer index
          history_buffer_idx[i] =
              (history_buffer_idx[i] + (uchar)1) & LZ77_history_buffer_idx_mask;  // TODO: is this fast enough ?!
        }
      }

      // update the most recent buffer
      history_buffer_buffer_idx =
          (history_buffer_buffer_idx + out_data.valid_count) & LZ77_history_buffer_buffer_idx_mask;

      // write the output to the pipe
      //write_channel_intel(ch_lz77_out, out_data); // TODO: retype
    } // if (!done && data_valid)
    
    out_data.flag = done;
    if(data_valid || done) {
      // write the output to the pipe
      write_channel_intel(ch_lz77_out, out_data); // TODO: retype
    }
		
  } // while (!done)

  //OutPipe::write(OutPipeBundleT(true)); // TODO
} // __kernel void LZ77DecoderMultiElement()


